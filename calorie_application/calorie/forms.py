from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm 
from django import forms
from django.contrib.auth.models import User
from .models import Food,Profile

#For user profile goal creation
class ProfileForm(forms.ModelForm):
	class Meta:
		model = Profile
		fields = ('calorie_goal',) 

#For registration of user
class CreateUserForm(UserCreationForm):
	class Meta:
		model = User
		fields = ['username','email','password1','password2']

#For food selection and quantity
class SelectFoodForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('selectionValue','quantity',)

    def __init__(self, user, *args, **kwargs):
        super(SelectFoodForm, self).__init__(*args, **kwargs)
        self.fields['selectionValue'].queryset = Food.objects.filter(person_of=user)

# Used to add new food products into the system
class AddFoodForm(forms.ModelForm):
    class Meta:
        model = Food
        fields = ('name','quantity','calorie')

   
