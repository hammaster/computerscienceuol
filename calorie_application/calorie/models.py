from django.db import models
from django.contrib.auth.models import User

from datetime import date

class Food(models.Model):
	name = models.CharField(max_length=200 ,null=False)
	person_of = models.ForeignKey(User,null=True,on_delete=models.CASCADE)
	quantity = models.PositiveIntegerField(null=False,default=0)
	calorie = models.FloatField(null=False,default=0)
	
	def __str__(self):
		return self.name


class Profile(models.Model):
	person_of = models.ForeignKey(User,null=True,on_delete=models.CASCADE)
	date = models.DateField(auto_now_add = True)
	calorie_goal = models.PositiveIntegerField(default=0)
	calorieValue = models.FloatField(default=0,null=True,blank=True)
	selectionValue= models.ForeignKey(Food,on_delete=models.CASCADE,null=True,blank=True)
	quantity = models.FloatField(default=0)
	allFoodSelectedCurr = models.ManyToManyField(Food,through='foodAdditon',related_name='inventory')
	total_calorie = models.FloatField(default=0,null=True)
	

	def save(self, *args, **kwargs):# new
		if self.selectionValue != None:
			self.amount = (self.selectionValue.calorie/self.selectionValue.quantity) 
			calories = Profile.objects.filter(person_of=self.person_of).last()
			self.calorieValue = self.amount*self.quantity
			self.total_calorie = self.calorieValue + self.total_calorie  	
			foodAdditon.objects.create(profile=calories,food=self.selectionValue,calorie_amount=self.calorieValue,amount=self.quantity)
			self.selectionValue = None
			super(Profile, self).save(*args,**kwargs)
	
		else:
			super(Profile,self).save(*args,**kwargs)

	def __str__(self):
		return str(self.person_of.username)


class foodAdditon(models.Model):
	profile = models.ForeignKey(Profile,on_delete=models.CASCADE)
	calorie_amount = models.FloatField(default=0,null=True,blank=True)
	food = models.ForeignKey(Food,on_delete=models.CASCADE)
	amount = models.FloatField(default=0)
												

