from django.urls import path
from .views import HomePageView,userLogin,userLogout, displayProfile,foodSelection,foodAddition,userRegistration,displayProfile, foodUpdate,foodDelete,workout


urlpatterns = [
 path('', HomePageView,name='home'),
 path('login/',userLogin,name='login'),
 path('logout/',userLogout,name='logout'),
 path('select_food/',foodSelection,name='select_food'),

 path('delete_food/<str:pk>/',foodDelete,name='delete_food'),
 path('registeration/',userRegistration,name='register'),
 path('profile/',displayProfile,name='profile'),

 path('workout/',workout,name='workout'),

 path('add_food/',foodAddition,name='add_food'),
 path('update_food/<str:pk>/', foodUpdate,name='update_food'),

]