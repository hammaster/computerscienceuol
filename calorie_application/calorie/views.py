from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login,logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import SelectFoodForm,AddFoodForm,CreateUserForm,ProfileForm
from .models import *
from datetime import timedelta
from django.utils import timezone
from datetime import date
from datetime import datetime
from .filters import FoodFilter

#User Homepage
@login_required(login_url='login')
def HomePageView(request):

	#using the latest profile as reference
	user_calorie = Profile.objects.filter(person_of=request.user).last()

	calorie_goal = user_calorie.calorie_goal
	
	#Creation of one profile each day
	if date.today() > user_calorie.date:
		profile=Profile.objects.create(person_of=request.user)
		profile.save()
	user_calorie = Profile.objects.filter(person_of=request.user).last()
	# showing all food consumed present day

	all_food_today=foodAdditon.objects.filter(profile=user_calorie)
	
	calorie_goal_status = calorie_goal -user_calorie.total_calorie
	over_calorie = 0
	if calorie_goal_status < 0 :
		over_calorie = abs(calorie_goal_status)

	context = {
	'total_calorie':user_calorie.total_calorie,
	'calorie_goal':calorie_goal,
	'calorie_goal_status':calorie_goal_status,
	'over_calorie' : over_calorie,
	'food_selected_today':all_food_today
	}
	
	return render(request, 'home.html',context)

#Loads the Profile Page
@login_required
def displayProfile(request):
	#This displays the profile from the user
	person = Profile.objects.filter(person_of=request.user).last()
	food_items = Food.objects.filter(person_of=request.user)
	form = ProfileForm(instance=person)

	if request.method == 'POST':
		form = ProfileForm(request.POST,instance=person)
		if form.is_valid():	
			form.save()
			return redirect('profile')
	else:
		form = ProfileForm(instance=person)

	#querying all records for the last seven days 
	some_day_last_week = timezone.now().date() -timedelta(days=7)
	records=Profile.objects.filter(date__gte=some_day_last_week,date__lt=timezone.now().date(),person_of=request.user)

	context = {'form':form,'food_items':food_items,'records':records}
	return render(request, 'profile.html',context)

#Registration Page 
def userRegistration(request):
	if request.user.is_authenticated:
		return redirect('home')
	else:
		form = CreateUserForm()
		if request.method == 'POST':
			form = CreateUserForm(request.POST)
			if form.is_valid():
				form.save()
				user = form.cleaned_data.get('username')
				return redirect('login')

		context = {'form':form}
		return render(request,'register.html',context)

#Login Page
def userLogin(request):
	if request.user.is_authenticated:
		return redirect('home')
	else:

		if request.method == 'POST':
			username = request.POST.get('username')
			password = request.POST.get('password')
			user = authenticate(request,username=username,password=password)
			if user is not None:
				login(request,user)
				return redirect('home')
			else:
				messages.info(request,'Username/Password incorrect')
		context = {}
		return render(request,'login.html',context)

#User Logout Page
def userLogout(request):
	logout(request)
	return redirect('login')

#Food Selection Daily
@login_required
def foodSelection(request):
	user = Profile.objects.filter(person_of=request.user).last()
	#Shows all food items available
	food_items = Food.objects.filter(person_of=request.user)
	form = SelectFoodForm(request.user,instance=user)

	if request.method == 'POST':
		form = SelectFoodForm(request.user,request.POST,instance=user)
		if form.is_valid():
			
			form.save()
			return redirect('home')
	else:
		form = SelectFoodForm(request.user)

	context = {'form':form,'food_items':food_items}
	return render(request, 'select_food.html',context)

#To allow user to add new food 
def foodAddition(request):
	#Displaying all food items
	food_items = Food.objects.filter(person_of=request.user)
	form = AddFoodForm(request.POST) 
	if request.method == 'POST':
		form = AddFoodForm(request.POST)
		if form.is_valid():
			profile = form.save(commit=False)
			profile.person_of = request.user
			profile.save()
			return redirect('add_food')
	else:
		form = AddFoodForm()
		
	#for filtering food
	myFoodFilter = FoodFilter(request.GET,queryset=food_items)
	food_items = myFoodFilter.qs
	context = {'form':form,'food_items':food_items,'myFilter':myFoodFilter}
	return render(request,'add_food.html',context)

#for updating food given by the user
@login_required
def foodUpdate(request,pk):
	foodItems = Food.objects.filter(person_of=request.user)

	food = Food.objects.get(id=pk)
	form =  AddFoodForm(instance=food )
	if request.method == 'POST':
		form = AddFoodForm(request.POST,instance=food )
		if form.is_valid():
			form.save()
			return redirect('profile')
	myFilter = FoodFilter(request.GET,queryset=foodItems)
	context = {'form':form,'food_items':foodItems,'myFilter':myFilter}

	return render(request,'add_food.html',context)

# To allow user to view workout routine videos
@login_required
def workout(request):
	
	

	return render(request,'workout.html')


#Deletion of Food From Database
@login_required
def foodDelete(request,pk):
	food = Food.objects.get(id=pk)
	if request.method == "POST":
		food.delete()
		return redirect('profile')
	context = {'food':food,}
	return render(request,'delete_food.html',context)


